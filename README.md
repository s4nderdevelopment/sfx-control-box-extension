# **SFX Control Box Extension**

Live on the fly tuning of all parameters in SimFeedback like min speed, max speed, acceleration, smoothness and intensity using ***any*** button box. Full control of all effects and the ability to save per profile button-configurations.

## **Installation**

1. Download and install the [SFX-100-Streamdeck PipeContract](https://github.com/ashupp/SFX-100-Streamdeck/releases) extension (***StreamdeckExtension.zip*** under *assets*).
2. Unpack the zip (extract here) in the SimFeedback *extensions* folder.
3. Download the *SFX Control Box Extension.zip* release from the [releases](https://gitlab.com/s4nderdevelopment/sfx-control-box-extension/-/releases) page.
4. Unpack the zip (extract here) in the SimFeedback *extensions* folder.
5. **Run unblock.bat** in the SimFeedback root folder.
6. Start SimFeedback, check activate and autostart in the setup tab for both extensions.
7. **Close and restart SimFeedback.**
8. *Enjoy*
9. If you would like to a second controller, follow the steps above again for *SFX Control Box Extension 2.zip*.

## **Manual**

1. Go to the extensions tab.
2. Select your joystick/button box in the dropdown.
3. Assign the buttons to the desired functions in the tabs.
4. Click save to save your button configuration for the active profile.
5. **To save your configuration for the active profile, click the *save* button**.
6. All available effects and controllers are automatically loaded from the XML. 
7. **If you would like to switch between two SimFeedback profiles without a full restart, you have manually load the profile in the extensions tab by clicking the *load* button.**
8. You can also load the saved configurations of another profile into the active profile by using the *Load from other profile* button and the corresponding dropdown menu.
9. The SFX Control Box Extension *will* work when switching two SimFeedback profiles without a restart if all the effects have exactly the same names.

![screenshot](Screenshot 1.png "Screenshot")

### **Link PipeContract**

Thanks to ashupp (@daCuJo) for creating the pipe contract server. Link to his GitHub code:
[SFX-100-Streamdeck PipeContract](https://github.com/ashupp/SFX-100-Streamdeck)