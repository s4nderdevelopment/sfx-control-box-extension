﻿using Microsoft.Win32.SafeHandles;
using sfx_100_streamdeck_pipecontract;
using SharpDX.DirectInput;
using SimFeedback.extension;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.ServiceModel;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Automation;
using System.Windows.Forms;
using System.Windows.Threading;
using System.Xml;

namespace SFXControlBoxExtension
{
    public class SFXControlBoxExtension : AbstractSimFeedbackExtension
    {
        [DllImport("kernel32")]
        static extern bool AllocConsole();

        [DllImport("kernel32")]
        static extern bool FreeConsole();

        [DllImport("kernel32.dll",
            EntryPoint = "GetStdHandle",
            SetLastError = true,
            CharSet = CharSet.Auto,
            CallingConvention = CallingConvention.StdCall)]
        private static extern IntPtr GetStdHandle(int nStdHandle);

        private const int STD_OUTPUT_HANDLE = -11;
        private const int MY_CODE_PAGE = 437;

        private bool isConsoleOpen = false;
        private bool debug = false;

        public bool IsConsoleOpen
        {
            get
            {
                return isConsoleOpen;
            }
        }

        private bool threadRunning = false;

        private Thread thread;

        public ISfxStreamDeckPipeContract channel;
        private NetNamedPipeBinding binding;
        private EndpointAddress ep;
        private static volatile SFXControlBoxExtension instance;
        private static object syncInstance = new object();
        private static object syncChannel = new object();
        private static object loadLock = new object();

        private ControlBoxCommandItem currentCommandItemToConfigure;
        private Button currentButtonToConfigure;

        private int debounceTimeMilliseconds = 500;

        private ControlBoxControl buttonBoxControl;

        private DirectInput dinput;

        public DirectInput DInput
        {
            get
            {
                return dinput;
            }
        }

        public override Control ExtensionControl => buttonBoxControl;

        private Joystick selectedJoystick = null;
        private Guid selectedJoystickGuid = Guid.Empty;

        public int DebounceTimeMilliseconds
        {
            get
            {
                return debounceTimeMilliseconds;
            }
        }

        public static SFXControlBoxExtension Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncInstance)
                    {
                        if (instance == null)
                        {
                            instance = new SFXControlBoxExtension();
                        }
                        return instance;
                    }
                }
                return instance;
            }
        }
        
        public void CreateServer()
        {
            string address = "net.pipe://localhost/ashnet/StreamDeckExtension";
            binding = new NetNamedPipeBinding(NetNamedPipeSecurityMode.None)
            {
                CloseTimeout = TimeSpan.FromSeconds(5),
                OpenTimeout = TimeSpan.FromSeconds(5),
                ReceiveTimeout = TimeSpan.FromSeconds(10),
                SendTimeout = TimeSpan.FromSeconds(5)
            };
            
            ep = new EndpointAddress(address);
            channel = ChannelFactory<ISfxStreamDeckPipeContract>.CreateChannel(binding, ep);
        }

        //TODO: Run this on start (and maybe stop)...
        public void ReloadJoystick()
        {
            if (!selectedJoystickGuid.Equals(Guid.Empty))
            {
                buttonBoxControl.RefreshJoysticksList();
                IList<DeviceInstance> devices = DInput.GetDevices();
                foreach (DeviceInstance device in devices)
                {
                    if (device.InstanceGuid.Equals(selectedJoystickGuid))
                    {
                        Joystick joystick = new Joystick(DInput, selectedJoystickGuid);
                        joystick.Acquire();
                        OnJoystickChange(joystick, selectedJoystickGuid);
                        buttonBoxControl.SetSelectedDevice(selectedJoystickGuid);
                        break;
                    }
                }
            }
        }
        
        public SFXControlBoxExtension()
        {
            instance = this;
            Name = "SFXControlBoxExtension";
            Info = "SFX Control Box Extension 2";
            Version = "0.7.6";
            Author = "S4nderDevelopment";
            HasControl = true;

            dinput = new DirectInput();

            Load();
        }
        
        public void RestartChannel()
        {
            try
            {
                LogMessage("State was: " + ((ICommunicationObject)channel).State);
                if (channel != null)
                {
                    ((ICommunicationObject)channel).Abort();
                    ((ICommunicationObject)channel).Close();
                }
                Log("Restarting Channel...!");
                CreateServer();
                LogMessage("State is: " + ((ICommunicationObject)channel).State);
                Log("Restart Channel Success!");
            }
            catch (Exception ex)
            {
                LogError("Error during RestartChannel: ", ex);
            }
        }

        public override void Start()
        {
            try
            {
                if (channel != null)
                {
                    ((ICommunicationObject)channel).Abort();
                    ((ICommunicationObject)channel).Close();
                }
                CreateServer();
                if (!threadRunning)
                {
                    threadRunning = true;
                    thread = new Thread(new ThreadStart(Run));
                    thread.Start();
                }
                if (!isConsoleOpen && debug)
                {
                    AllocConsole();
                    IntPtr stdHandle = GetStdHandle(STD_OUTPUT_HANDLE);
                    SafeFileHandle safeFileHandle = new SafeFileHandle(stdHandle, true);
                    FileStream fileStream = new FileStream(safeFileHandle, FileAccess.Write);
                    Encoding encoding = Encoding.GetEncoding(MY_CODE_PAGE);
                    StreamWriter standardOutput = new StreamWriter(fileStream, encoding);
                    standardOutput.AutoFlush = true;
                    Console.SetOut(standardOutput);
                    isConsoleOpen = true;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error during RestartChannel: " + ex.Message);
            }

            var mainWindowHandle = Process.GetCurrentProcess().MainWindowHandle;

            if ((int)mainWindowHandle != 0)
            {
                Dispatcher currentDispatcher = Dispatcher.CurrentDispatcher;

                Task.Run(async () =>
                {
                    await Task.Delay(1500);

                    currentDispatcher.Invoke(() =>
                    {
                        AutomationElement root = AutomationElement.FromHandle(Application.OpenForms[0].Handle);
                        
                        AutomationElement simFeedbackTreeView = root.FindFirst(TreeScope.Subtree, new PropertyCondition(AutomationElement.AutomationIdProperty, "profileView"));

                        AddProfileChangeDetector(simFeedbackTreeView);
                    });
                });
            }
            
            LogMessage("SFX Control Box Extension started.");
        }

        public override void Stop()
        {
            try
            {
                if (channel != null)
                {
                    ((ICommunicationObject)channel).Abort();
                    ((ICommunicationObject)channel).Close();
                }

                if (threadRunning)
                {
                    threadRunning = false;
                    thread.Join();
                    thread = null;
                }
                if (isConsoleOpen && debug)
                {
                    isConsoleOpen = false;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error during RestartChannel: " + ex.Message);
            }
            LogMessage("SFX Control Box Extension stopped.");
        }

        public void OnJoystickButtonChangeButtonClick(ControlBoxCommandItem item, Button clickedButton)
        {
            if (currentButtonToConfigure == null)
            {
                currentButtonToConfigure = clickedButton;
                currentCommandItemToConfigure = item;
                clickedButton.BackColor = Color.Yellow;
                clickedButton.Text = "Press a button on your joystick...";
            }
            else
            {
                currentButtonToConfigure.BackColor = Color.LightGray;
                currentButtonToConfigure.Text = "Not configured (click to change)";
                currentCommandItemToConfigure.JoystickButton.JoystickButton = -1;
                currentButtonToConfigure = null;
                currentCommandItemToConfigure = null;
            }
            //clickedButton.BackColor = Color.LightGreen;
        }

        public void OnJoystickChange(Joystick joystick, Guid instanceGuid)
        {
            selectedJoystickGuid = instanceGuid;
            selectedJoystick = joystick;
        }

        public int GetActiveButton(JoystickState state)
        {
            for (int i = 0; i < state.Buttons.Length; i++)
            {
                if (state.Buttons[i])
                {
                    return i;
                }
            }
            return -1;
        }

        public void Run()
        {
            DateTime lastChangedButtonTime = DateTime.Now;

            int keepAliveTimer = 0;

            while (threadRunning)
            {
                try
                {
                    lock (syncChannel)
                    {
                        JoystickState state = new JoystickState();

                        selectedJoystick.Acquire();

                        selectedJoystick.Poll();

                        selectedJoystick.GetCurrentState(ref state);

                        if (currentButtonToConfigure != null)
                        {
                            int pressedBtn = GetActiveButton(state);
                            if (pressedBtn >= 0)
                            {
                                bool alreadyAssigned = false;
                                foreach (ControlBoxCommandItem item in buttonBoxControl.CommandItemsCommon)
                                {
                                    if (item.JoystickButton.JoystickButton == pressedBtn)
                                    {
                                        alreadyAssigned = true;
                                    }
                                }
                                foreach (var column in buttonBoxControl.CommandItemsEffectNames)
                                {
                                    foreach (ControlBoxCommandItem item in column)
                                    {
                                        if (item.JoystickButton.JoystickButton == pressedBtn)
                                        {
                                            alreadyAssigned = true;
                                        }
                                    }
                                }
                                foreach (var column in buttonBoxControl.CommandItemsControllers)
                                {
                                    foreach (ControlBoxCommandItem item in column)
                                    {
                                        if (item.JoystickButton.JoystickButton == pressedBtn)
                                        {
                                            alreadyAssigned = true;
                                        }
                                    }
                                }
                                if (!alreadyAssigned)
                                {
                                    currentCommandItemToConfigure.JoystickButton.JoystickButton = pressedBtn;
                                    currentButtonToConfigure.BackColor = Color.LightGreen;
                                    currentButtonToConfigure.Text = "Button " + currentCommandItemToConfigure.JoystickButton.JoystickButton + " (click to change)";
                                    currentButtonToConfigure = null;
                                    currentCommandItemToConfigure = null;
                                    lastChangedButtonTime = DateTime.Now;
                                }
                                else
                                {
                                    currentButtonToConfigure.BackColor = Color.LightGray;
                                    currentButtonToConfigure.Text = "Not configured (click to change)";
                                    currentCommandItemToConfigure.JoystickButton.JoystickButton = -1;
                                    currentButtonToConfigure = null;
                                    currentCommandItemToConfigure = null;
                                    MessageBox.Show("Button already in use.");
                                }
                            }
                            if ((keepAliveTimer % (6 * 60 * 100)) == (6 * 60 * 100 - 1))
                            {
                                keepAliveTimer = 0;
                                channel.GetOverallIntensity();
                            }
                            else
                            {
                                keepAliveTimer++;
                            }
                        }
                        else
                        {
                            if ((DateTime.Now - lastChangedButtonTime).TotalMilliseconds > 2000)
                            {
                                //Handle button joystick presses here...
                                for (int i = 0; i < state.Buttons.Length; i++)
                                {
                                    if (state.Buttons[i])
                                    {
                                        foreach (ControlBoxCommandItem item in buttonBoxControl.CommandItemsCommon)
                                        {
                                            item.OnButtonPress(i);
                                        }
                                        foreach (var column in buttonBoxControl.CommandItemsEffectNames)
                                        {
                                            foreach (ControlBoxCommandItem item in column)
                                            {
                                                item.OnButtonPress(i);
                                            }
                                        }
                                        foreach (var column in buttonBoxControl.CommandItemsControllers)
                                        {
                                            foreach (ControlBoxCommandItem item in column)
                                            {
                                                item.OnButtonPress(i);
                                            }
                                        }
                                    }
                                }
                            }
                            if ((keepAliveTimer % (6 * 60 * 100)) == (6 * 60 * 100 - 1))
                            {
                                keepAliveTimer = 0;
                                channel.GetOverallIntensity();
                            }
                            else
                            {
                                keepAliveTimer++;
                            }
                        }
                        /*
                        if (channel.CheckConnection())
                        {
                            int overallIntensity = channel.GetOverallIntensity();
                            if (overallIntensity <= 90)
                            {
                                overallIntensity += 10;
                            }
                            else
                            {
                                overallIntensity = 0;
                            }
                            channel.SetOverallIntensity(overallIntensity);
                        }*/
                    }

                    Thread.Sleep(10);
                }
                catch (Exception)
                {
                    Thread.Sleep(100);
                }
            }
        }
        
        public void GetControllersAndEffects(List<string> controllerNames, List<string> effectNames)
        {
            XmlDocument simFeedbackXMLDoc = new XmlDocument();
            simFeedbackXMLDoc.Load(Path.Combine(Directory.GetCurrentDirectory(), "SimFeedback.xml"));

            XmlNodeList nodes = simFeedbackXMLDoc.SelectNodes("/ConfigData/MotionControllerList/MotionControllerConfigData/type");

            foreach (XmlNode node in nodes)
            {
                controllerNames.Add(node.InnerText);
            }

            foreach (string fileName in Directory.GetFiles(Path.Combine(Directory.GetCurrentDirectory(), "profiles")))
            {
                string profileXML = File.ReadAllText(fileName);

                for (int i = 0; i < 10; i++)
                {
                    try
                    {
                        XmlDocument pluginXMLDoc = new XmlDocument();
                        pluginXMLDoc.LoadXml(profileXML);

                        XmlNode rootNode = pluginXMLDoc.SelectSingleNode("/Profile");
                        if (rootNode.Attributes["Active"].Value.ToLower() == "true")
                        {
                            XmlNodeList effectNameNodes = rootNode.SelectNodes("FeedbackEffectList/FeedbackEffect/name");

                            foreach (XmlNode effectNameNode in effectNameNodes)
                            {
                                effectNames.Add(effectNameNode.InnerText);
                            }
                        }
                        break;
                    }
                    catch (Exception)
                    {
                        profileXML = profileXML.Substring(0, profileXML.Length - 1);
                    }
                }
            }
        }
        
        public void Generate()
        {
            try
            {
                List<string> controllerNames = new List<string>();
                List<string> effectNames = new List<string>();

                GetControllersAndEffects(controllerNames, effectNames);
                
                List<ControlBoxCommandItem> buttonBoxCommandItemsCommon = new List<ControlBoxCommandItem>();
                List<List<ControlBoxCommandItem>> buttonBoxCommandItemsEffectNames = new List<List<ControlBoxCommandItem>>();
                List<List<ControlBoxCommandItem>> buttonBoxCommandItemsControllers = new List<List<ControlBoxCommandItem>>();

                ControlBoxCommandItem buttonBoxCommandItem = new ControlBoxCommandItem();
                buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "IncrementOverallIntensity"), 1, (int value) =>
                {
                    if (channel.CheckConnection())
                    {
                        return channel.IncrementOverallIntensity(value);
                    }
                    return null;
                });

                buttonBoxCommandItemsCommon.Add(buttonBoxCommandItem);

                buttonBoxCommandItem = new ControlBoxCommandItem();
                buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "DecrementOverallIntensity"), 1, (int value) =>
                {
                    if (channel.CheckConnection())
                    {
                        return channel.DecrementOverallIntensity(value);
                    }
                    return null;
                });

                buttonBoxCommandItemsCommon.Add(buttonBoxCommandItem);

                buttonBoxCommandItem = new ControlBoxCommandItem();
                buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "StartMotion"), () =>
                {
                    if (channel.CheckConnection())
                    {
                        return channel.StartMotion();
                    }
                    return null;
                });

                buttonBoxCommandItemsCommon.Add(buttonBoxCommandItem);

                buttonBoxCommandItem = new ControlBoxCommandItem();
                buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "StopMotion"), () =>
                {
                    if (channel.CheckConnection())
                    {
                        return channel.StopMotion();
                    }
                    return null;
                });

                buttonBoxCommandItemsCommon.Add(buttonBoxCommandItem);
                
                buttonBoxCommandItem = new ControlBoxCommandItem();
                buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EnableAllEffects"), () =>
                {
                    if (channel.CheckConnection())
                    {
                        return channel.EnableAllEffects();
                    }
                    return null;
                });

                buttonBoxCommandItemsCommon.Add(buttonBoxCommandItem);
                
                buttonBoxCommandItem = new ControlBoxCommandItem();
                buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "DisableAllEffects"), () =>
                {
                    if (channel.CheckConnection())
                    {
                        return channel.DisableAllEffects();
                    }
                    return null;
                });

                buttonBoxCommandItemsCommon.Add(buttonBoxCommandItem);
                
                
                buttonBoxCommandItem = new ControlBoxCommandItem();
                buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "SetOverallIntensity"), 10, (int value) =>
                {
                    if (channel.CheckConnection())
                    {
                        return channel.SetOverallIntensity(value);
                    }
                    return null;
                });

                buttonBoxCommandItemsCommon.Add(buttonBoxCommandItem);

                for (int i = 0; i < controllerNames.Count; i++)
                {
                    List<ControlBoxCommandItem> buttonBoxCommandItemsColumn = new List<ControlBoxCommandItem>();

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerIntensityIncrement"), controllerNames, 1, (string controllerName, int value) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.ControllerIntensityIncrement(controllerName, value);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = controllerNames[i];
                    
                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerIntensityDecrement"), controllerNames, 1, (string controllerName, int value) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.ControllerIntensityDecrement(controllerName, value);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = controllerNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerIntensitySet"), controllerNames, 50, (string controllerName, int value) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.ControllerIntensitySet(controllerName, value);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = controllerNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerSmoothnessIncrement"), controllerNames, 1, (string controllerName, int value) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.ControllerSmoothnessIncrement(controllerName, value);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = controllerNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerSmoothnessDecrement"), controllerNames, 1, (string controllerName, int value) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.ControllerSmoothnessDecrement(controllerName, value);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = controllerNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerSmoothnessSet"), controllerNames, 50, (string controllerName, int value) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.ControllerSmoothnessSet(controllerName, value);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = controllerNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerSmoothnessEnable"), controllerNames, (string controllerName) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.ControllerSmoothnessEnable(controllerName);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = controllerNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerSmoothnessDisable"), controllerNames, (string controllerName) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.ControllerSmoothnessDisable(controllerName);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = controllerNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerSmoothnessToggle"), controllerNames, (string controllerName) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.ControllerSmoothnessToggle(controllerName);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = controllerNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerAccelerationIncrement"), controllerNames, 1, (string controllerName, int value) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.ControllerAccelerationIncrement(controllerName, value);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = controllerNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerAccelerationDecrement"), controllerNames, 1, (string controllerName, int value) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.ControllerAccelerationDecrement(controllerName, value);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = controllerNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerAccelerationSet"), controllerNames, 50, (string controllerName, int value) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.ControllerAccelerationSet(controllerName, value);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = controllerNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerMinSpeedIncrement"), controllerNames, 1, (string controllerName, int value) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.ControllerMinSpeedIncrement(controllerName, value);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = controllerNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerMinSpeedDecrement"), controllerNames, 1, (string controllerName, int value) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.ControllerMinSpeedDecrement(controllerName, value);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = controllerNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerMinSpeedSet"), controllerNames, 50, (string controllerName, int value) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.ControllerMinSpeedSet(controllerName, value);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = controllerNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerMaxSpeedIncrement"), controllerNames, 1, (string controllerName, int value) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.ControllerMaxSpeedIncrement(controllerName, value);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = controllerNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerMaxSpeedDecrement"), controllerNames, 1, (string controllerName, int value) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.ControllerMaxSpeedDecrement(controllerName, value);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = controllerNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerMaxSpeedSet"), controllerNames, 50, (string controllerName, int value) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.ControllerMaxSpeedSet(controllerName, value);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = controllerNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItemsControllers.Add(buttonBoxCommandItemsColumn);
                }

                for (int i = 0; i < effectNames.Count; i++)
                {
                    List<ControlBoxCommandItem> buttonBoxCommandItemsColumn = new List<ControlBoxCommandItem>();
                    
                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectSmoothingIncrement"), effectNames, 1, (string effectName, int value) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.EffectSmoothingIncrement(effectName, value);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = effectNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectSmoothingDecrement"), effectNames, 1, (string effectName, int value) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.EffectSmoothingDecrement(effectName, value);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = effectNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectSmoothingSet"), effectNames, 50, (string effectName, int value) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.EffectSmoothingSet(effectName, value);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = effectNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectIntensityIncrement"), effectNames, 1, (string effectName, int value) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.EffectIntensityIncrement(effectName, value);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = effectNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectIntensityDecrement"), effectNames, 1, (string effectName, int value) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.EffectIntensityDecrement(effectName, value);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = effectNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectIntensitySet"), effectNames, 50, (string effectName, int value) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.EffectIntensitySet(effectName, value);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = effectNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectLinearToggle"), effectNames, (string effectName) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.EffectLinearToggle(effectName);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = effectNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectToggle"), effectNames, (string effectName) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.EffectToggle(effectName);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = effectNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);
                    
                    buttonBoxCommandItem = new ControlBoxCommandItem();
                    buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectMuteToggle"), effectNames, (string effectName) =>
                    {
                        if (channel.CheckConnection())
                        {
                            return channel.EffectMuteToggle(effectName);
                        }
                        return null;
                    });
                    buttonBoxCommandItem.SelectedEffectName = effectNames[i];

                    buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

                    buttonBoxCommandItemsEffectNames.Add(buttonBoxCommandItemsColumn);
                }

                /////////////////////////////

                if (buttonBoxControl == null)
                {
                    buttonBoxControl = new ControlBoxControl();
                }
                buttonBoxControl.Configure(buttonBoxCommandItemsCommon, buttonBoxCommandItemsEffectNames, buttonBoxCommandItemsControllers, controllerNames, effectNames);
                
                LogMessage("Done generating default config");
            }
            catch (Exception ex)
            {
                LogError("Error loading default config", ex);
                throw ex;
            }
        }

        public List<ControlBoxCommandItem> CreateControllerTab(string name, List<string> controllerNames)
        {
            List<ControlBoxCommandItem> buttonBoxCommandItemsColumn = new List<ControlBoxCommandItem>();

            ControlBoxCommandItem buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerIntensityIncrement"), controllerNames, 1, (string controllerName, int value) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.ControllerIntensityIncrement(controllerName, value);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerIntensityDecrement"), controllerNames, 1, (string controllerName, int value) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.ControllerIntensityDecrement(controllerName, value);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerIntensitySet"), controllerNames, 50, (string controllerName, int value) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.ControllerIntensitySet(controllerName, value);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerSmoothnessIncrement"), controllerNames, 1, (string controllerName, int value) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.ControllerSmoothnessIncrement(controllerName, value);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerSmoothnessDecrement"), controllerNames, 1, (string controllerName, int value) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.ControllerSmoothnessDecrement(controllerName, value);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerSmoothnessSet"), controllerNames, 50, (string controllerName, int value) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.ControllerSmoothnessSet(controllerName, value);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerSmoothnessEnable"), controllerNames, (string controllerName) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.ControllerSmoothnessEnable(controllerName);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerSmoothnessDisable"), controllerNames, (string controllerName) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.ControllerSmoothnessDisable(controllerName);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerSmoothnessToggle"), controllerNames, (string controllerName) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.ControllerSmoothnessToggle(controllerName);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerAccelerationIncrement"), controllerNames, 1, (string controllerName, int value) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.ControllerAccelerationIncrement(controllerName, value);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerAccelerationDecrement"), controllerNames, 1, (string controllerName, int value) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.ControllerAccelerationDecrement(controllerName, value);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerAccelerationSet"), controllerNames, 50, (string controllerName, int value) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.ControllerAccelerationSet(controllerName, value);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerMinSpeedIncrement"), controllerNames, 1, (string controllerName, int value) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.ControllerMinSpeedIncrement(controllerName, value);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerMinSpeedDecrement"), controllerNames, 1, (string controllerName, int value) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.ControllerMinSpeedDecrement(controllerName, value);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerMinSpeedSet"), controllerNames, 50, (string controllerName, int value) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.ControllerMinSpeedSet(controllerName, value);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerMaxSpeedIncrement"), controllerNames, 1, (string controllerName, int value) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.ControllerMaxSpeedIncrement(controllerName, value);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerMaxSpeedDecrement"), controllerNames, 1, (string controllerName, int value) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.ControllerMaxSpeedDecrement(controllerName, value);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "ControllerMaxSpeedSet"), controllerNames, 50, (string controllerName, int value) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.ControllerMaxSpeedSet(controllerName, value);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);
            
            return buttonBoxCommandItemsColumn;
        }
        
        public List<ControlBoxCommandItem> CreateEffectTab(string name, List<string> effectNames)
        {
            List<ControlBoxCommandItem> buttonBoxCommandItemsColumn = new List<ControlBoxCommandItem>();
            
            ControlBoxCommandItem buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectSmoothingIncrement"), effectNames, 1, (string effectName, int value) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.EffectSmoothingIncrement(effectName, value);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectSmoothingDecrement"), effectNames, 1, (string effectName, int value) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.EffectSmoothingDecrement(effectName, value);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectSmoothingSet"), effectNames, 50, (string effectName, int value) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.EffectSmoothingSet(effectName, value);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectIntensityIncrement"), effectNames, 1, (string effectName, int value) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.EffectIntensityIncrement(effectName, value);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectIntensityDecrement"), effectNames, 1, (string effectName, int value) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.EffectIntensityDecrement(effectName, value);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectIntensitySet"), effectNames, 50, (string effectName, int value) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.EffectIntensitySet(effectName, value);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectLinearEnable"), effectNames, (string effectName) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.EffectLinearEnable(effectName);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectLinearDisable"), effectNames, (string effectName) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.EffectLinearDisable(effectName);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectLinearToggle"), effectNames, (string effectName) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.EffectLinearToggle(effectName);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectEnable"), effectNames, (string effectName) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.EffectEnable(effectName);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectDisable"), effectNames, (string effectName) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.EffectDisable(effectName);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectToggle"), effectNames, (string effectName) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.EffectToggle(effectName);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectMuteEnable"), effectNames, (string effectName) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.EffectMuteEnable(effectName);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectMuteDisable"), effectNames, (string effectName) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.EffectMuteDisable(effectName);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            buttonBoxCommandItem = new ControlBoxCommandItem();
            buttonBoxCommandItem.Configure(new ConfiguredJoystickButton(-1, "EffectMuteToggle"), effectNames, (string effectName) =>
            {
                if (channel.CheckConnection())
                {
                    return channel.EffectMuteToggle(effectName);
                }
                return null;
            });
            buttonBoxCommandItem.SelectedEffectName = name;

            buttonBoxCommandItemsColumn.Add(buttonBoxCommandItem);

            return buttonBoxCommandItemsColumn;
        }

        public List<string> GetAllProfileNames()
        {
            List<string> result = new List<string>();
            foreach (string fileName in Directory.GetFiles(Path.Combine(Directory.GetCurrentDirectory(), "profiles")))
            {
                string xmlDoc = File.ReadAllText(fileName);
                for (int i = 0; i < 10; i++)
                {
                    try
                    {
                        XmlDocument pluginXMLDoc = new XmlDocument();
                        pluginXMLDoc.LoadXml(xmlDoc);

                        XmlNode rootNode = pluginXMLDoc.SelectSingleNode("/Profile");
                        result.Add(rootNode.Attributes["Name"].Value);
                        break;
                    }
                    catch (Exception)
                    {
                        xmlDoc = xmlDoc.Substring(0, xmlDoc.Length - 1);
                    }
                }
            }

            return result;
        }

        public string GetActiveProfileName()
        {
            foreach (string fileName in Directory.GetFiles(Path.Combine(Directory.GetCurrentDirectory(), "profiles")))
            {
                string xmlDoc = File.ReadAllText(fileName);
                for (int i = 0; i < 10; i++)
                {
                    try
                    {
                        XmlDocument pluginXMLDoc = new XmlDocument();
                        pluginXMLDoc.LoadXml(xmlDoc);

                        XmlNode rootNode = pluginXMLDoc.SelectSingleNode("/Profile");
                        if (rootNode.Attributes["Active"].Value == "true")
                        {
                            return rootNode.Attributes["Name"].Value;
                        }
                        break;
                    }
                    catch (Exception)
                    {
                        xmlDoc = xmlDoc.Substring(0, xmlDoc.Length - 1);
                    }
                }
            }

            return null;
        }

        public int Save()
        {
            int errorCount = 0;
            try
            {
                string profileName = GetActiveProfileName();
                string currentDir = Directory.GetCurrentDirectory();
                string buttonBoxDir = Path.Combine(currentDir, "SFXControlBoxExtension");
                if (!Directory.Exists(buttonBoxDir))
                {
                    Directory.CreateDirectory(buttonBoxDir);
                }
                string profileFileName = Path.Combine(buttonBoxDir, profileName + "-bbox.xml");

                var commonItems = buttonBoxControl.CommandItemsCommon;
                var controllerItems = buttonBoxControl.CommandItemsControllers;
                var effectNamesItems = buttonBoxControl.CommandItemsEffectNames;

                XmlDocument doc = new XmlDocument();

                XmlNode node = doc.CreateElement("RootNode");

                XmlNode lastJoystickGuidNode = doc.CreateElement("LastGuid");
                lastJoystickGuidNode.InnerText = selectedJoystickGuid.ToString();
                node.AppendChild(lastJoystickGuidNode);

                foreach (var item in commonItems)
                {
                    XmlNode itemNode = doc.CreateElement("CommonCommandItem");

                    XmlNode buttonNode = doc.CreateElement("Button");
                    buttonNode.InnerText = "" + item.JoystickButton.JoystickButton;
                    itemNode.AppendChild(buttonNode);

                    XmlNode commandNameNode = doc.CreateElement("CommandName");
                    commandNameNode.InnerText = "" + item.JoystickButton.CommandName;
                    itemNode.AppendChild(commandNameNode);

                    if (item.ItemType == ControlBoxCommandItem.CommandItemType.WithValue || item.ItemType == ControlBoxCommandItem.CommandItemType.WithEffectNameAndValue)
                    {
                        XmlNode valueNode = doc.CreateElement("Value");
                        valueNode.InnerText = "" + item.Value;
                        itemNode.AppendChild(valueNode);
                    }

                    node.AppendChild(itemNode);
                }
                int index = 0;
                foreach (var controllerColumn in controllerItems)
                {
                    XmlNode controllerNode = doc.CreateElement("Controllers");

                    string tabName = buttonBoxControl.ControllerTabNames[index];

                    XmlNode tabNameElement = doc.CreateElement("TabName");

                    tabNameElement.InnerText = tabName;

                    controllerNode.AppendChild(tabNameElement);

                    foreach (var item in controllerColumn)
                    {
                        XmlNode itemNode = doc.CreateElement("ControllerCommandItem");

                        XmlNode buttonNode = doc.CreateElement("Button");
                        buttonNode.InnerText = "" + item.JoystickButton.JoystickButton;
                        itemNode.AppendChild(buttonNode);

                        XmlNode commandNameNode = doc.CreateElement("CommandName");
                        commandNameNode.InnerText = "" + item.JoystickButton.CommandName;
                        itemNode.AppendChild(commandNameNode);

                        if (item.ItemType == ControlBoxCommandItem.CommandItemType.WithEffectName || item.ItemType == ControlBoxCommandItem.CommandItemType.WithEffectNameAndValue)
                        {
                            XmlNode effectNameNode = doc.CreateElement("Controller");
                            effectNameNode.InnerText = item.SelectedEffectName;
                            itemNode.AppendChild(effectNameNode);
                        }

                        if (item.ItemType == ControlBoxCommandItem.CommandItemType.WithValue || item.ItemType == ControlBoxCommandItem.CommandItemType.WithEffectNameAndValue)
                        {
                            XmlNode valueNode = doc.CreateElement("Value");
                            valueNode.InnerText = "" + item.Value;
                            itemNode.AppendChild(valueNode);
                        }
                        controllerNode.AppendChild(itemNode);
                    }
                    node.AppendChild(controllerNode);
                    index++;
                }
                index = 0;
                foreach (var effectNamesColumn in effectNamesItems)
                {
                    XmlNode effectNode = doc.CreateElement("EffectNames");

                    string tabName = buttonBoxControl.EffectTabNames[index];

                    XmlNode tabNameElement = doc.CreateElement("TabName");

                    tabNameElement.InnerText = tabName;

                    effectNode.AppendChild(tabNameElement);

                    foreach (var item in effectNamesColumn)
                    {
                        XmlNode itemNode = doc.CreateElement("EffectNameCommandItem");

                        XmlNode buttonNode = doc.CreateElement("Button");
                        buttonNode.InnerText = "" + item.JoystickButton.JoystickButton;
                        itemNode.AppendChild(buttonNode);

                        XmlNode commandNameNode = doc.CreateElement("CommandName");
                        commandNameNode.InnerText = "" + item.JoystickButton.CommandName;
                        itemNode.AppendChild(commandNameNode);

                        if (item.ItemType == ControlBoxCommandItem.CommandItemType.WithEffectName || item.ItemType == ControlBoxCommandItem.CommandItemType.WithEffectNameAndValue)
                        {
                            XmlNode effectNameNode = doc.CreateElement("EffectName");
                            effectNameNode.InnerText = item.SelectedEffectName;
                            itemNode.AppendChild(effectNameNode);
                        }

                        if (item.ItemType == ControlBoxCommandItem.CommandItemType.WithValue || item.ItemType == ControlBoxCommandItem.CommandItemType.WithEffectNameAndValue)
                        {
                            XmlNode valueNode = doc.CreateElement("Value");
                            valueNode.InnerText = "" + item.Value;
                            itemNode.AppendChild(valueNode);
                        }
                        effectNode.AppendChild(itemNode);
                    }
                    node.AppendChild(effectNode);
                    index++;
                }

                doc.AppendChild(node);
                doc.Save(profileFileName);
            }
            catch (Exception e)
            {
                LogError("Could not save the config", e);
                errorCount = -1;
            }
            return errorCount;
        }
        
        public int Load(string profileToCopyFrom = null)
        {
            string profileFileName;

            int errorCount = 0;

            string profileName = profileToCopyFrom == null ? GetActiveProfileName() : profileToCopyFrom;
            string currentDir = Directory.GetCurrentDirectory();
            string buttonBoxDir = Path.Combine(currentDir, "SFXControlBoxExtension");
            if (!Directory.Exists(buttonBoxDir))
            {
                Directory.CreateDirectory(buttonBoxDir);
            }

            List<string> possibleControllerNames = new List<string>();
            List<string> possibleEffectNames = new List<string>();
            GetControllersAndEffects(possibleControllerNames, possibleEffectNames);

            profileFileName = Path.Combine(buttonBoxDir, profileName + "-bbox.xml");
            
            if (File.Exists(profileFileName))
            {
                XmlDocument doc = new XmlDocument();
                doc.Load(profileFileName);
                
                try
                {
                    List<ControlBoxCommandItem> commandItemsCommon = new List<ControlBoxCommandItem>();
                    List<List<ControlBoxCommandItem>> commandItemsController = new List<List<ControlBoxCommandItem>>();
                    List<List<ControlBoxCommandItem>> commandItemsEffect = new List<List<ControlBoxCommandItem>>();

                    List<string> controllerNameTabNames = new List<string>();
                    List<string> effectNameTabNames = new List<string>();

                    XmlNode lastJoystickGuidNode = doc.SelectSingleNode("/RootNode/LastGuid");
                    selectedJoystickGuid = Guid.Parse(lastJoystickGuidNode.InnerText);
                    
                    XmlNodeList commonCommandItems = doc.SelectNodes("/RootNode/CommonCommandItem");
                    XmlNodeList controllerCommandItemsColumns = doc.SelectNodes("/RootNode/Controllers");
                    XmlNodeList effectNamesCommandItemsColumns = doc.SelectNodes("/RootNode/EffectNames");

                    foreach (XmlNode commonCommandItem in commonCommandItems)
                    {
                        //item
                        try
                        {
                            int button = 0;
                            string commandName = "";
                            button = Convert.ToInt32(commonCommandItem.SelectSingleNode("Button").InnerText);
                            commandName = commonCommandItem.SelectSingleNode("CommandName").InnerText;
                            
                            XmlNodeList valueNodeList = commonCommandItem.SelectNodes("Value");
                            bool hasValue = valueNodeList.Count > 0;

                            ConfiguredJoystickButton joystickButton = new ConfiguredJoystickButton(button, commandName);

                            if (!hasValue)
                            {
                                ControlBoxCommandItem commandItemCommon = new ControlBoxCommandItem();
                                commandItemCommon.Configure(joystickButton, () =>
                                {
                                    MethodInfo info = channel.GetType().GetMethod(commandName);
                                    return info.Invoke(channel, new object[] { });
                                });
                                commandItemsCommon.Add(commandItemCommon);
                            }
                            else
                            {
                                int loadedValue = Convert.ToInt32(valueNodeList[0].InnerText);
                                ControlBoxCommandItem commandItemCommon = new ControlBoxCommandItem();
                                commandItemCommon.Configure(joystickButton, loadedValue, (int value) =>
                                {
                                    MethodInfo info = channel.GetType().GetMethod(commandName);
                                    return info.Invoke(channel, new object[] { value });
                                });
                                commandItemsCommon.Add(commandItemCommon);
                            }
                        }
                        catch (Exception e)
                        {
                            errorCount++;
                            LogError("Failed to load profile button configuration for one common button mapping.", e);
                        }
                    }
                    foreach (XmlNode controllerCommandItemColumn in controllerCommandItemsColumns)
                    {
                        List<ControlBoxCommandItem> commandItemsControllerColumn = new List<ControlBoxCommandItem>();
                        //column
                        string controllerNameTabName = controllerCommandItemColumn.SelectSingleNode("TabName").InnerText;
                        
                        foreach (XmlNode controllerCommandItem in controllerCommandItemColumn.SelectNodes("ControllerCommandItem"))
                        {
                            //item
                            try
                            {
                                int button = 0;
                                string commandName = "";
                                string loadedControllerName = "";
                                button = Convert.ToInt32(controllerCommandItem.SelectSingleNode("Button").InnerText);
                                commandName = controllerCommandItem.SelectSingleNode("CommandName").InnerText;
                                loadedControllerName = controllerCommandItem.SelectSingleNode("Controller").InnerText;
                                
                                XmlNodeList valueNodeList = controllerCommandItem.SelectNodes("Value");
                                bool hasValue = valueNodeList.Count > 0;
                                
                                ConfiguredJoystickButton joystickButton = new ConfiguredJoystickButton(button, commandName);

                                if (!hasValue)
                                {
                                    ControlBoxCommandItem commandItemCommon = new ControlBoxCommandItem();
                                    commandItemCommon.Configure(joystickButton, possibleControllerNames, (string controllerName) =>
                                    {
                                        MethodInfo info = channel.GetType().GetMethod(commandName);
                                        return info.Invoke(channel, new object[] { controllerName });
                                    });
                                    commandItemCommon.SelectedEffectName = loadedControllerName;
                                    commandItemsControllerColumn.Add(commandItemCommon);
                                }
                                else
                                {
                                    int loadedValue = Convert.ToInt32(valueNodeList[0].InnerText);
                                    ControlBoxCommandItem commandItemController = new ControlBoxCommandItem();
                                    commandItemController.Configure(joystickButton, possibleControllerNames, loadedValue, (string controllerName, int value) =>
                                    {
                                        MethodInfo info = channel.GetType().GetMethod(commandName);
                                        return info.Invoke(channel, new object[] { controllerName, value });
                                    });
                                    commandItemController.SelectedEffectName = loadedControllerName;
                                    commandItemsControllerColumn.Add(commandItemController);
                                }
                            }
                            catch (Exception e)
                            {
                                errorCount++;
                                LogError("Failed to load profile button configuration for one controller button mapping.", e);
                            }
                        }
                        commandItemsController.Add(commandItemsControllerColumn);
                        controllerNameTabNames.Add(controllerNameTabName);
                    }
                    foreach (XmlNode effectNamesCommandItemColumn in effectNamesCommandItemsColumns)
                    {
                        List<ControlBoxCommandItem> commandItemsEffectColumn = new List<ControlBoxCommandItem>();
                        //column
                        string effectNameTabName = effectNamesCommandItemColumn.SelectSingleNode("TabName").InnerText;
                        foreach (XmlNode effectNamesCommandItem in effectNamesCommandItemColumn.SelectNodes("EffectNameCommandItem"))
                        {
                            //item
                            try
                            {
                                int button = 0;
                                string commandName = "";
                                string loadedEffectName = "";
                                button = Convert.ToInt32(effectNamesCommandItem.SelectSingleNode("Button").InnerText);
                                commandName = effectNamesCommandItem.SelectSingleNode("CommandName").InnerText;
                                loadedEffectName = effectNamesCommandItem.SelectSingleNode("EffectName").InnerText;

                                XmlNodeList valueNodeList = effectNamesCommandItem.SelectNodes("Value");
                                bool hasValue = valueNodeList.Count > 0;

                                ConfiguredJoystickButton joystickButton = new ConfiguredJoystickButton(button, commandName);

                                if (!hasValue)
                                {
                                    ControlBoxCommandItem commandItemEffect = new ControlBoxCommandItem();
                                    commandItemEffect.Configure(joystickButton, possibleEffectNames, (string effectName) =>
                                    {
                                        MethodInfo info = channel.GetType().GetMethod(commandName);
                                        return info.Invoke(channel, new object[] { effectName });
                                    });
                                    commandItemEffect.SelectedEffectName = loadedEffectName;
                                    commandItemsEffectColumn.Add(commandItemEffect);
                                }
                                else
                                {
                                    int loadedValue = Convert.ToInt32(valueNodeList[0].InnerText);
                                    ControlBoxCommandItem commandItemEffect = new ControlBoxCommandItem();
                                    commandItemEffect.Configure(joystickButton, possibleEffectNames, loadedValue, (string effectName, int value) =>
                                    {
                                        MethodInfo info = channel.GetType().GetMethod(commandName);
                                        return info.Invoke(channel, new object[] { effectName, value });
                                    });
                                    commandItemEffect.SelectedEffectName = loadedEffectName;
                                    commandItemsEffectColumn.Add(commandItemEffect);
                                }
                            }
                            catch (Exception e)
                            {
                                errorCount++;
                                LogError("Failed to load profile button configuration for one effect button mapping.", e);
                            }
                        }
                        commandItemsEffect.Add(commandItemsEffectColumn);
                        effectNameTabNames.Add(effectNameTabName);
                    }
                    
                    if (buttonBoxControl == null)
                    {
                        buttonBoxControl = new ControlBoxControl();
                    }
                    LogMessage("Done loading profile button configurations.");
                    buttonBoxControl.Configure(commandItemsCommon, commandItemsEffect, commandItemsController, controllerNameTabNames, effectNameTabNames);

                    ReloadJoystick();
                }
                catch (Exception e)
                {
                    LogError("Failed to load profile button configurations, skipping load...", e);
                    errorCount = -1;
                }
            }
            else
            {
                Generate();
            }
            return errorCount;
        }

        public void LogMessage(string message)
        {
            Console.WriteLine(message);
        }

        public void LogError(string message, Exception exception = null)
        {
            string toSend = "An exception occurred in the SFX Control Box Extension: " + message;
            if (exception != null && debug)
            {
                toSend += "\nException: " + exception.Message + "\nStackTrace: " + exception.StackTrace;
            }
            Console.Error.WriteLine(toSend);
        }
        
        private void AddProfileChangeDetector(AutomationElement simFeedbackTreeView)
        {
            Dispatcher thisThreadDispatcher = Dispatcher.CurrentDispatcher;
            try
            {
                Automation.AddStructureChangedEventHandler(simFeedbackTreeView, TreeScope.Element, delegate (object o, StructureChangedEventArgs args)
                {
                    AutomationElement element = o as AutomationElement;

                    if (args.StructureChangeType == StructureChangeType.ChildRemoved || args.StructureChangeType == StructureChangeType.ChildrenBulkRemoved)
                    {
                        LogMessage("Profile change, loading config...");
                        thisThreadDispatcher.Invoke(() =>
                        {
                            AutomationElement root = AutomationElement.FromHandle(Application.OpenForms[0].Handle);

                            AutomationElement simFeedbackTreeView2 = root.FindFirst(TreeScope.Subtree, new PropertyCondition(AutomationElement.AutomationIdProperty, "profileView"));

                            LogMessage("Re-adding profile change detector");
                            
                            AddProfileChangeDetector(simFeedbackTreeView2);

                            LogMessage("Re-added profile change detector");

                            Load();
                        });
                    }
                });
            }
            catch (Exception ex)
            {
                LogError("Error during loading of AddProfileChangeDetector", ex);
            }
        }
    }
}
