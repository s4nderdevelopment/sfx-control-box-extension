﻿using SharpDX.DirectInput;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SFXControlBoxExtension
{
    public class ControlBoxControl : Panel
    {
        private ComboBox joystickComboBox;

        private List<ControlBoxCommandItem> commandItemsCommon;
        private List<List<ControlBoxCommandItem>> commandItemsEffectNames;
        private List<List<ControlBoxCommandItem>> commandItemsControllers;

        private TabControl tabControl;
        
        public List<ControlBoxCommandItem> CommandItemsCommon
        {
            get
            {
                return commandItemsCommon;
            }
        }

        public List<List<ControlBoxCommandItem>> CommandItemsEffectNames
        {
            get
            {
                return commandItemsEffectNames;
            }
        }
        
        public List<List<ControlBoxCommandItem>> CommandItemsControllers
        {
            get
            {
                return commandItemsControllers;
            }
        }

        public TabControl TabControl
        {
            get
            {
                return tabControl;
            }
        }

        public List<string> ControllerTabNames { get; private set; }
        public List<string> EffectTabNames { get; private set; }
        
        public void SetSelectedDevice(Guid guid)
        {
            foreach (NamedDeviceInstance instance in joystickComboBox.Items)
            {
                if (instance.Instance.InstanceGuid.Equals(guid))
                {
                    joystickComboBox.SelectedItem = instance;
                    break;
                }
            }
        }

        public ControlBoxControl()
        {
            joystickComboBox = new ComboBox();
            ControllerTabNames = new List<string>();
            EffectTabNames = new List<string>();
            commandItemsCommon = new List<ControlBoxCommandItem>();
            commandItemsEffectNames = new List<List<ControlBoxCommandItem>>();
            commandItemsControllers = new List<List<ControlBoxCommandItem>>();
            tabControl = new TabControl();
        }
        
        public void Configure(List<ControlBoxCommandItem> commandItemsCommon, List<List<ControlBoxCommandItem>> commandItemsEffectNames, List<List<ControlBoxCommandItem>> commandItemsControllers, List<string> controllerNames, List<string> effectNames)
        {
            ResetConfiguration();
            Label extensionName = new Label();
            extensionName.Size = new Size(800, 40);
            extensionName.Location = new Point(5, 5);
            extensionName.TextAlign = ContentAlignment.TopLeft;
            extensionName.Text = "SFX Control Box Extension";
            extensionName.Font = new Font(new FontFamily("Arial"), 20.0f, FontStyle.Bold);
            this.Controls.Add(extensionName);

            LinkLabel credits = new LinkLabel();
            credits.Size = new Size(800, 20);
            credits.Location = new Point(5, 45);
            credits.TextAlign = ContentAlignment.BottomLeft;
            credits.Text = "By S4nderDevelopment";
            credits.Links.Add(new LinkLabel.Link(3, credits.Text.Length));
            credits.LinkClicked += (object sender, LinkLabelLinkClickedEventArgs eventArgs) =>
            {
                System.Diagnostics.Process.Start("https://gitlab.com/S4nderDevelopment");
            };
            credits.Font = new Font(new FontFamily("Arial"), 12.0f, FontStyle.Bold | FontStyle.Italic);
            this.Controls.Add(credits);
            
            Label label = new Label();
            label.Size = new Size(200, 40);
            label.Location = new Point(5, 75);
            label.TextAlign = ContentAlignment.MiddleLeft;
            label.Text = "Select a joystick: ";
            this.Controls.Add(label);

            joystickComboBox = new ComboBox();
            joystickComboBox.Size = new Size(400, 40);
            joystickComboBox.Location = new Point(215, 75 + (40 - joystickComboBox.Size.Height) / 2);
            
            IList<DeviceInstance> devices = SFXControlBoxExtension.Instance.DInput.GetDevices();

            foreach (DeviceInstance device in devices)
            {
                joystickComboBox.Items.Add(new NamedDeviceInstance(device));
            }
            joystickComboBox.SelectedIndexChanged += (object sender, EventArgs e) =>
            {
                DeviceInstance device = (joystickComboBox.SelectedItem as NamedDeviceInstance).Instance;
                if (device != null)
                {
                    try
                    {
                        Joystick joystick = new Joystick(SFXControlBoxExtension.Instance.DInput, device.InstanceGuid);

                        joystick.Acquire();
                        SFXControlBoxExtension.Instance.OnJoystickChange(joystick, device.InstanceGuid);
                    }
                    catch (Exception)
                    {
                        joystickComboBox.Items.Clear();

                        IList<DeviceInstance> devices2 = SFXControlBoxExtension.Instance.DInput.GetDevices();

                        foreach (DeviceInstance device2 in devices2)
                        {
                            joystickComboBox.Items.Add(new NamedDeviceInstance(device2));
                        }
                    }
                }
            };

            this.Controls.Add(joystickComboBox);

            Button refreshJoysticksButton = new Button();
            refreshJoysticksButton.Size = new Size(200, 40);
            refreshJoysticksButton.Location = new Point(635, 75);
            refreshJoysticksButton.Text = "Refresh joysticks list";
            refreshJoysticksButton.Click += (object sender, EventArgs e) =>
            {
                RefreshJoysticksList();
            };

            this.Controls.Add(refreshJoysticksButton);

            Button saveButton = new Button();
            saveButton.Size = new Size(200, 60);
            saveButton.Location = new Point(5, 125);
            saveButton.Text = "Save";
            saveButton.Click += (object sender, EventArgs e) =>
            {
                int loadErrorCount = SFXControlBoxExtension.Instance.Save();
                if (loadErrorCount == 0)
                {
                    saveButton.BackColor = Color.LightGreen;
                }
            };

            this.Controls.Add(saveButton);

            Button loadButton = new Button();
            loadButton.Size = new Size(200, 60);
            loadButton.Location = new Point(215, 125);
            loadButton.Text = "Load";
            loadButton.Click += (object sender, EventArgs e) =>
            {
                DialogResult result = MessageBox.Show("Changes will be lost if not saved!", "Are you sure?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    int loadErrorCount = SFXControlBoxExtension.Instance.Load();
                }
            };
            
            this.Controls.Add(loadButton);

            ComboBox profilesList = new ComboBox();
            profilesList.Size = new Size(200, 20);
            profilesList.Location = new Point(635, 165);
            
            foreach (string profile in SFXControlBoxExtension.Instance.GetAllProfileNames())
            {
                profilesList.Items.Add(profile);
            }
            
            this.Controls.Add(profilesList);
            
            Button loadFromOtherProfileButton = new Button();
            loadFromOtherProfileButton.Size = new Size(200, 30);
            loadFromOtherProfileButton.Location = new Point(635, 125);
            loadFromOtherProfileButton.Text = "Load from other profile";
            loadFromOtherProfileButton.Click += (object sender, EventArgs e) =>
            {
                DialogResult result = MessageBox.Show("Changes will be lost if not saved!", "Are you sure?", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Warning);
                if (result == DialogResult.Yes)
                {
                    if (profilesList.SelectedItem != null)
                    {
                        int loadErrorCount = SFXControlBoxExtension.Instance.Load(profilesList.SelectedItem as string);
                    }
                    else
                    {
                        MessageBox.Show("Please select a profile.");
                    }
                }
            };
            
            this.Controls.Add(loadFromOtherProfileButton);

            int highestY = 0;

            int y = 0;
            TabPage page = new TabPage("Common");

            tabControl.TabPages.Add(page);

            foreach (ControlBoxCommandItem item in commandItemsCommon)
            {
                this.commandItemsCommon.Add(item);
                page.Controls.Add(item);
                item.Location = new Point(0, y * 50);
                y++;
            }
            if (y > highestY)
            {
                highestY = y;
            }
            page.Size = new Size(850, y * 50);

            int index = 0;

            foreach (List<ControlBoxCommandItem> commandItemsColumn in commandItemsControllers)
            {
                string pageText = "Controller: " + controllerNames[index];
                page = new TabPage(pageText);
                y = 0;
                foreach (ControlBoxCommandItem item in commandItemsColumn)
                {
                    page.Controls.Add(item);
                    item.Location = new Point(0, y * 50);
                    y++;
                }

                Button deleteTabButton = new Button();
                deleteTabButton.Text = "Delete tab";
                deleteTabButton.Click += (object s, EventArgs ev) =>
                {
                    DialogResult result = MessageBox.Show("Do you want to delete this tab?", "Are you sure?", MessageBoxButtons.YesNoCancel);
                    if (result == DialogResult.Yes)
                    {
                        foreach (TabPage tab in tabControl.TabPages)
                        {
                            if (tab.Text == pageText)
                            {
                                tabControl.TabPages.Remove(tab);

                                List<ControlBoxCommandItem> listToRemove = null;

                                int i = 0;
                                
                                foreach (List<ControlBoxCommandItem> items in this.commandItemsControllers)
                                {
                                    bool equal = true;
                                    foreach (var item in commandItemsColumn)
                                    {
                                        if (!items.Contains(item))
                                        {
                                            equal = false;
                                        }
                                    }
                                    if (equal)
                                    {
                                        listToRemove = items;
                                        break;
                                    }
                                    i++;
                                }

                                if (listToRemove != null)
                                {
                                    this.commandItemsControllers.Remove(listToRemove);
                                    this.ControllerTabNames.RemoveAt(i);
                                }

                                return;
                            }
                        }
                    }
                };
                deleteTabButton.Size = new Size(790, 40);
                deleteTabButton.Location = new Point(5, y * 50 + 5);
                deleteTabButton.BackColor = Color.Red;

                page.Controls.Add(deleteTabButton);
                
                if (y > highestY)
                {
                    highestY = y;
                }
                this.commandItemsControllers.Add(commandItemsColumn);
                this.ControllerTabNames.Add(controllerNames[index]);

                tabControl.TabPages.Add(page);

                index++;
            }

            index = 0;

            foreach (List<ControlBoxCommandItem> commandItemsColumn in commandItemsEffectNames)
            {
                string pageText = "Effect: " + effectNames[index];
                page = new TabPage(pageText);
                y = 0;
                foreach (ControlBoxCommandItem item in commandItemsColumn)
                {
                    page.Controls.Add(item);
                    item.Location = new Point(0, y * 50);
                    y++;
                }

                Button deleteTabButton = new Button();
                deleteTabButton.Text = "Delete tab";
                deleteTabButton.Click += (object s, EventArgs ev) =>
                {
                    DialogResult result = MessageBox.Show("Do you want to delete this tab?", "Are you sure?", MessageBoxButtons.YesNoCancel);
                    if (result == DialogResult.Yes)
                    {
                        foreach (TabPage tab in tabControl.TabPages)
                        {
                            if (tab.Text == pageText)
                            {
                                tabControl.TabPages.Remove(tab);

                                List<ControlBoxCommandItem> listToRemove = null;

                                int i = 0;

                                foreach (List<ControlBoxCommandItem> items in this.commandItemsEffectNames)
                                {
                                    bool equal = true;
                                    foreach (var item in commandItemsColumn)
                                    {
                                        if (!items.Contains(item))
                                        {
                                            equal = false;
                                        }
                                    }
                                    if (equal)
                                    {
                                        listToRemove = items;
                                        break;
                                    }
                                    i++;
                                }

                                if (listToRemove != null)
                                {
                                    this.commandItemsEffectNames.Remove(listToRemove);
                                    this.EffectTabNames.RemoveAt(i);
                                }

                                return;
                            }
                        }
                    }
                };
                deleteTabButton.Size = new Size(790, 40);
                deleteTabButton.Location = new Point(5, y * 50 + 5);
                deleteTabButton.BackColor = Color.Red;

                page.Controls.Add(deleteTabButton);

                if (y > highestY)
                {
                    highestY = y;
                }
                this.commandItemsEffectNames.Add(commandItemsColumn);
                this.EffectTabNames.Add(effectNames[index]);

                tabControl.TabPages.Add(page);
                index++;
            }

            List<string> allControllerNames = new List<string>();

            List<string> allEffectNames = new List<string>();
            SFXControlBoxExtension.Instance.GetControllersAndEffects(allControllerNames, allEffectNames);
            
            ComboBox createControllerComboBox = new ComboBox();
            foreach (string controllerName in allControllerNames)
            {
                createControllerComboBox.Items.Add(controllerName);
            }
            createControllerComboBox.Size = new Size(410, 30);
            createControllerComboBox.Location = new Point(5, 235);
            
            Button createControllerButton = new Button();
            createControllerButton.Text = "Add new controller";
            createControllerButton.Size = new Size(410, 30);
            createControllerButton.Location = new Point(5, 195);
            createControllerButton.Click += (object sender, EventArgs e) =>
            {
                object selectedControllerName = createControllerComboBox.SelectedItem;
                if (selectedControllerName != null) {

                    string controllerName = selectedControllerName as string;

                    foreach(TabPage tab in tabControl.TabPages)
                    {
                        if(tab.Text == "Controller: " + controllerName)
                        {
                            MessageBox.Show("Controller tab already exists.");
                            return;
                        }
                    }

                    var commandItemsColumn = SFXControlBoxExtension.Instance.CreateControllerTab(controllerName, allControllerNames);

                    page = new TabPage("Controller: " + controllerName);
                    y = 0;
                    foreach (ControlBoxCommandItem item in commandItemsColumn)
                    {
                        page.Controls.Add(item);
                        item.Location = new Point(0, y * 50);
                        y++;
                    }

                    Button deleteTabButton = new Button();
                    deleteTabButton.Text = "Delete tab";
                    deleteTabButton.Click += (object s, EventArgs ev) =>
                    {
                        DialogResult result = MessageBox.Show("Do you want to delete this tab?", "Are you sure?", MessageBoxButtons.YesNoCancel);
                        if (result == DialogResult.Yes)
                        {
                            foreach (TabPage tab in tabControl.TabPages)
                            {
                                if (tab.Text == "Controller: " + controllerName)
                                {
                                    tabControl.TabPages.Remove(tab);

                                    List<ControlBoxCommandItem> listToRemove = null;

                                    int i = 0;

                                    foreach (List<ControlBoxCommandItem> items in this.commandItemsControllers)
                                    {
                                        bool equal = true;
                                        foreach (var item in commandItemsColumn)
                                        {
                                            if (!items.Contains(item))
                                            {
                                                equal = false;
                                            }
                                        }
                                        if (equal)
                                        {
                                            listToRemove = items;
                                            break;
                                        }
                                        i++;
                                    }

                                    if (listToRemove != null)
                                    {
                                        this.commandItemsControllers.Remove(listToRemove);
                                        this.ControllerTabNames.RemoveAt(i);
                                    }

                                    return;
                                }
                            }
                        }
                    };
                    deleteTabButton.Size = new Size(790, 40);
                    deleteTabButton.Location = new Point(5, y * 50 + 5);
                    deleteTabButton.BackColor = Color.Red;

                    page.Controls.Add(deleteTabButton);
                    
                    this.commandItemsControllers.Insert(0, commandItemsColumn);
                    this.ControllerTabNames.Insert(0, controllerName);
                    
                    tabControl.TabPages.Insert(1, page);
                }
                else
                {
                    MessageBox.Show("Please select a controller.");
                }
            };
            
            ComboBox createEffectComboBox = new ComboBox();
            foreach (string effectName in allEffectNames)
            {
                createEffectComboBox.Items.Add(effectName);
            }
            createEffectComboBox.Size = new Size(410, 30);
            createEffectComboBox.Location = new Point(425, 235);
            
            Button createEffectButton = new Button();
            createEffectButton.Text = "Add new effect";
            createEffectButton.Size = new Size(410, 30);
            createEffectButton.Location = new Point(425, 195);
            createEffectButton.Click += (object sender, EventArgs e) =>
            {
                object selectedEffectName = createEffectComboBox.SelectedItem;
                if (selectedEffectName != null) {
                    string effectName = selectedEffectName as string;

                    foreach (TabPage tab in tabControl.TabPages)
                    {
                        if (tab.Text == "Effect: " + effectName)
                        {
                            MessageBox.Show("Effect tab already exists.");
                            return;
                        }
                    }
                    
                    var commandItemsColumn = SFXControlBoxExtension.Instance.CreateEffectTab(effectName, allEffectNames);
                    
                    page = new TabPage("Effect: " + effectName);
                    y = 0;
                    foreach (ControlBoxCommandItem item in commandItemsColumn)
                    {
                        page.Controls.Add(item);
                        item.Location = new Point(0, y * 50);
                        y++;
                    }
                    
                    Button deleteTabButton = new Button();
                    deleteTabButton.Text = "Delete tab";
                    deleteTabButton.Click += (object s, EventArgs ev) =>
                    {
                        DialogResult result = MessageBox.Show("Do you want to delete this tab?", "Are you sure?", MessageBoxButtons.YesNoCancel);
                        if (result == DialogResult.Yes)
                        {
                            foreach (TabPage tab in tabControl.TabPages)
                            {
                                if (tab.Text == "Effect: " + effectName)
                                {
                                    tabControl.TabPages.Remove(tab);

                                    List<ControlBoxCommandItem> listToRemove = null;

                                    int i = 0;
                                    
                                    foreach (List<ControlBoxCommandItem> items in this.commandItemsEffectNames)
                                    {
                                        bool equal = true;
                                        foreach (var item in commandItemsColumn)
                                        {
                                            if (!items.Contains(item))
                                            {
                                                equal = false;
                                            }
                                        }
                                        if (equal)
                                        {
                                            listToRemove = items;
                                            break;
                                        }
                                        i++;
                                    }

                                    if (listToRemove != null)
                                    {
                                        this.commandItemsEffectNames.Remove(listToRemove);
                                        this.EffectTabNames.RemoveAt(i);
                                    }

                                    return;
                                }
                            }
                        }
                    };
                    deleteTabButton.Size = new Size(790, 40);
                    deleteTabButton.Location = new Point(5, y * 50 + 5);
                    deleteTabButton.BackColor = Color.Red;

                    page.Controls.Add(deleteTabButton);
                    
                    this.commandItemsEffectNames.Add(commandItemsColumn);
                    this.EffectTabNames.Add(effectName);
                    
                    tabControl.TabPages.Add(page);
                }
                else
                {
                    MessageBox.Show("Please select an effect.");
                }
            };

            this.Controls.Add(createControllerComboBox);
            this.Controls.Add(createControllerButton);
            this.Controls.Add(createEffectComboBox);
            this.Controls.Add(createEffectButton);
            
            tabControl.Size = new Size(840, highestY * 50 + 150);
            tabControl.Location = new Point(0, 275);
            tabControl.Padding = new Point(0, 0);
            
            this.Controls.Add(tabControl);

            this.HScroll = true;
            this.AutoScroll = true;

            int maxHeight = 600;
            this.VScroll = true;
            this.Size = new Size(900, maxHeight);
            this.BackColor = Color.FromArgb(241, 241, 241);
            //this.BackColor = Color.FromArgb(227, 227, 227);

            EffectTabNames = effectNames;
            ControllerTabNames = controllerNames;
        }

        private void ResetConfiguration()
        {
            this.Controls.Clear();
            commandItemsCommon = new List<ControlBoxCommandItem>();
            foreach(var common in commandItemsCommon)
            {
                common.ResetConfiguration();
            }
            commandItemsEffectNames = new List<List<ControlBoxCommandItem>>();
            foreach (var effectsColumn in commandItemsEffectNames)
            {
                foreach (var effectName in effectsColumn)
                {
                    effectName.ResetConfiguration();
                }
            }
            commandItemsControllers = new List<List<ControlBoxCommandItem>>();
            foreach (var controllerColumn in commandItemsControllers)
            {
                foreach (var controller in controllerColumn)
                {
                    controller.ResetConfiguration();
                }
            }
            tabControl = new TabControl();
        }

        public void SelectJoystick(Guid guid)
        {
            foreach(var item in joystickComboBox.Items)
            {
                NamedDeviceInstance device = item as NamedDeviceInstance;
                if(device != null)
                {
                    if (device.Instance.InstanceGuid.Equals(joystickComboBox))
                    {
                        joystickComboBox.SelectedItem = device;
                    }
                }
            }
        }

        public void RefreshJoysticksList()
        {
            joystickComboBox.Items.Clear();

            IList<DeviceInstance> devices2 = SFXControlBoxExtension.Instance.DInput.GetDevices();

            foreach (DeviceInstance device2 in devices2)
            {
                joystickComboBox.Items.Add(new NamedDeviceInstance(device2));
            }
        }
    }
}
