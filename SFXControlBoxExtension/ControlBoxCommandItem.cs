﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SFXControlBoxExtension
{
    public class ControlBoxCommandItem : Panel
    {
        public enum CommandItemType
        {
            NoArgs,
            WithEffectName,
            WithValue,
            WithEffectNameAndValue
        }
        
        public delegate object ButtonClickMethodDelegate();
        public delegate object ButtonClickMethodWithEffectNameDelegate(string effectName);
        public delegate object ButtonClickMethodWithValueDelegate(int value);
        public delegate object ButtonClickMethodWithEffectNameAndValueDelegate(string effectName, int value);
        
        private ConfiguredJoystickButton joystickButton;
        private List<string> possibleEffectNames;
        private int value;

        public int Value
        {
            get
            {
                return value;
            }
        }
        
        public ConfiguredJoystickButton JoystickButton
        {
            get
            {
                return joystickButton;
            }
        }

        private ButtonClickMethodDelegate delegateFunc;
        private ButtonClickMethodWithEffectNameDelegate delegateFuncWithEffectName;
        private ButtonClickMethodWithValueDelegate delegateFuncWithValue;
        private ButtonClickMethodWithEffectNameAndValueDelegate delegateFuncWithEffectNameAndValue;

        private CommandItemType commandItemType;
        
        public CommandItemType ItemType
        {
            get
            {
                return commandItemType;
            }
        }

        private DateTime lastPressTime;

        private bool doDebounce = true;

        private string lastSelectedEffectName = null;

        public string SelectedEffectName
        {
            get
            {
                if (effectNamesComboBox.SelectedItem == null)
                {
                    return null;
                }
                return effectNamesComboBox.SelectedItem.ToString();
            }
            set
            {
                lastSelectedEffectName = value;
                if (effectNamesComboBox != null)
                {
                    foreach (var item in effectNamesComboBox.Items)
                    {
                        if (item.ToString() == value)
                        {
                            effectNamesComboBox.SelectedItem = item;
                        }
                    }
                }
            }
        }
        
        private ComboBox effectNamesComboBox;
        private NumericUpDown valueNumericUpDown;

        public ControlBoxCommandItem()
        {
            commandItemType = CommandItemType.NoArgs;
            lastPressTime = DateTime.Now;

            /////////////////////////
            //Configure(new ConfiguredJoystickButton(1, "Test"), new List<string>() { "Effect 1", "Effect 2" }, 10, (string effectName, int value) =>
            //{
            //    Console.WriteLine(effectName + " - " + value);
            //    return null;
            //});
        }
        
        public void Configure(ConfiguredJoystickButton joystickButton, ButtonClickMethodDelegate delegateFunc)
        {
            commandItemType = CommandItemType.NoArgs;
            this.joystickButton = joystickButton;
            this.delegateFunc = delegateFunc;
            OnInit();
        }

        public void Configure(ConfiguredJoystickButton joystickButton, List<string> possibleEffectNames, ButtonClickMethodWithEffectNameDelegate delegateFunc)
        {
            commandItemType = CommandItemType.WithEffectName;
            this.joystickButton = joystickButton;
            this.possibleEffectNames = possibleEffectNames;
            this.delegateFuncWithEffectName = delegateFunc;
            OnInit();
        }

        public void Configure(ConfiguredJoystickButton joystickButton, int value, ButtonClickMethodWithValueDelegate delegateFunc)
        {
            commandItemType = CommandItemType.WithValue;
            this.joystickButton = joystickButton;
            this.value = value;
            this.delegateFuncWithValue = delegateFunc;
            OnInit();
        }

        public void Configure(ConfiguredJoystickButton joystickButton, List<string> possibleEffectNames, int value, ButtonClickMethodWithEffectNameAndValueDelegate delegateFunc)
        {
            commandItemType = CommandItemType.WithEffectNameAndValue;
            this.joystickButton = joystickButton;
            this.possibleEffectNames = possibleEffectNames;
            this.value = value;
            this.delegateFuncWithEffectNameAndValue = delegateFunc;
            OnInit();
        }

        public void OnButtonPress(int button)
        {
            if (button == joystickButton.JoystickButton)
            {
                DateTime currentTime = DateTime.Now;
                if ((currentTime - lastPressTime).TotalMilliseconds > SFXControlBoxExtension.Instance.DebounceTimeMilliseconds || !doDebounce)
                {
                    lastPressTime = currentTime;

                    bool isConnected = SFXControlBoxExtension.Instance.channel.CheckConnection();
                    if (!isConnected)
                    {
                        Console.WriteLine("\n\n\nReconnecting...\n\n\n");
                    }
                    SFXControlBoxExtension.Instance.RestartChannel();

                    if (commandItemType == CommandItemType.NoArgs)
                    {
                        delegateFunc();
                    }
                    else if (commandItemType == CommandItemType.WithEffectName)
                    {
                        delegateFuncWithEffectName(effectNamesComboBox.SelectedItem.ToString());
                    }
                    else if (commandItemType == CommandItemType.WithValue)
                    {
                        delegateFuncWithValue((int)valueNumericUpDown.Value);
                    }
                    else if (commandItemType == CommandItemType.WithEffectNameAndValue)
                    {
                        delegateFuncWithEffectNameAndValue(effectNamesComboBox.SelectedItem.ToString(), (int)valueNumericUpDown.Value);
                    }
                }
            }
        }
        
        private void OnInit()
        {
            ResetConfiguration();
            Label nameLabel = new Label();
            nameLabel.Text = joystickButton.CommandName;
            nameLabel.AutoSize = false;
            nameLabel.Size = new Size(190, 40);
            nameLabel.Location = new Point(10, 5);
            this.Controls.Add(nameLabel);

            int configuredJoystickButton = joystickButton.JoystickButton;

            Button changeJoystickButtonBtn = new Button();
            if (configuredJoystickButton >= 0)
            {
                changeJoystickButtonBtn.Text = "Button " + configuredJoystickButton + " (click to change)";
                changeJoystickButtonBtn.BackColor = Color.LightGreen;
            }
            else
            {
                changeJoystickButtonBtn.Text = "Not configured (click to change)";
                changeJoystickButtonBtn.BackColor = Color.LightGray;
            }
            changeJoystickButtonBtn.Size = new Size(190, 40);
            changeJoystickButtonBtn.Location = new Point(210, 5);
            changeJoystickButtonBtn.Click += (object sender, EventArgs e) =>
            {
                SFXControlBoxExtension.Instance.OnJoystickButtonChangeButtonClick(this, changeJoystickButtonBtn);
            };
            
            this.Controls.Add(changeJoystickButtonBtn);

            if(commandItemType == CommandItemType.WithEffectName || commandItemType == CommandItemType.WithEffectNameAndValue)
            {
                effectNamesComboBox = new ComboBox();
                foreach(string effectName in possibleEffectNames)
                {
                    effectNamesComboBox.Items.Add(effectName);
                }
                effectNamesComboBox.Size = new Size(190, 40);
                effectNamesComboBox.Location = new Point(410, 5);
                effectNamesComboBox.MouseWheel += Ctl_MouseWheel;

                foreach (string effectName in effectNamesComboBox.Items)
                {
                    if (effectName == lastSelectedEffectName)
                    {
                        effectNamesComboBox.SelectedItem = effectName;
                    }
                }
                
                this.Controls.Add(effectNamesComboBox);
            }

            if(commandItemType == CommandItemType.WithValue || commandItemType == CommandItemType.WithEffectNameAndValue)
            {
                valueNumericUpDown = new NumericUpDown();
                valueNumericUpDown.Value = value;
                valueNumericUpDown.DecimalPlaces = 0;
                valueNumericUpDown.Size = new Size(190, 40);
                valueNumericUpDown.Location = new Point(610, 5);
                valueNumericUpDown.MouseWheel += Ctl_MouseWheel;

                valueNumericUpDown.ValueChanged += (object sender, EventArgs e) =>
                {
                    this.value = Convert.ToInt32(valueNumericUpDown.Value);
                };

                this.Controls.Add(valueNumericUpDown);
            }
            
            this.Size = new Size(800, 50);
        }
        private void Ctl_MouseWheel(object sender, MouseEventArgs e)
        {
            ((HandledMouseEventArgs)e).Handled = true;
        }

        public void ResetConfiguration()
        {
            this.Controls.Clear();
        }
    }
}
