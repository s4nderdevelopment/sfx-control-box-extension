﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFXControlBoxExtension
{
    public class ConfiguredJoystickButton
    {
        public int JoystickButton { get; set; }
        public string CommandName { get; set; }
        
        public ConfiguredJoystickButton(int joystickButton, string commandName)
        {
            this.JoystickButton = joystickButton;
            this.CommandName = commandName;
        }
    }
}
