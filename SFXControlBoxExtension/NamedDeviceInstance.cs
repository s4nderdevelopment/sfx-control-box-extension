﻿using SharpDX.DirectInput;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFXControlBoxExtension
{
    public class NamedDeviceInstance
    {
        public DeviceInstance Instance;
        public string Name
        {
            get
            {
                return Instance.InstanceName + " - " + Instance.ProductName;
            }
        }

        public NamedDeviceInstance(DeviceInstance instance)
        {
            this.Instance = instance;
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
